<h1>Création des manifests IIIF </h1>

:warning:
- Pour les manifests correspondant à une **donnée** Nakala, privilégier l'utilisation d'[ok-api](https://gitlab.com/litt-arts-num/fonte-gaia/ok-api) : métadonnées et infos sur les images peuvent être récupérées automatiquement par l'API Nakala.
- Pour les plus anciens, suivre la procédure dans l'ordre
  1. génération avec preziiif
  2. correction
  3. génération de la Table Of Content (toc)


<h2>Création des manifests avec preziiif (outdated)</h2>

* Fichiers pré-requis en entrée :
    1. Fichier de correspondance entre nom de l'image et identifiant ark (à construire lors du dépôt des données sur Nakala)  : `filename2handle.csv` (une ligne par image, sous la forme : <code>"11280/a7f5d2d2","384212101_XFP042_1920_Image00253.jpg"</code>)
    2. Liste des métadonnées : `metadonnees.csv` (une ligne d'entête : <code>dcterms:contributor,dcterms:creator,dcterms:date,dcterms:description,dcterms:format,dcterms:language,dcterms:publisher,dcterms:rights,dcterms:source,dcterms:subject,dcterms:identifier,dcterms:title,dcterms:type,dcterms:hasFormat</code> et une ligne par manuscrit : <code>"Doderet, André (1879-1949). Traducteur","D'Annunzio, Gabriele (1863-1938)",1928,,,fre| ita,Calmann-Lévy,Domaine public,SID UGA INP BU Droit et Lettres ,Théâtre (genre littéraire) italien -- 20e siècle -- Traductions françaises,384212101_122319_1927_196,La torche sous le boisseau,texte| monographie imprimée,http://fontegaia.huma-num.fr/files/manifests_json/384212101_122319_1927_196/manifest.json</code>)
    3. Images sources dans un dossier `loc_images/` contenant un dossier par manuscrit (portant l'identifiant `dcterms:identifier` du manuscrit en question) lequel contient un dossier `JPEG/` contenant les images au format .jpg. [Rem. Les fichiers ayant pour nom "Titre.jpg" seront ignorées, en effet, ces images sont utiles pour les vignettes Omeka uniquement)
* Configurer le script `manifests.py` en spécifiant les valeurs adéquates pour <code>loc_images</code>, <code>base_uri </code>, <code>metada</code> et `stock_manifest`
* Lancer grâce à la commande `python3 manifests.py >rapportCreationManifest.txt`. Attendre patiemment.

Le fichier rapportCreationManifest.txt contient éventuellement des WARNING (erreurs plutôt dûes aux libraries Python) ou ATTENTION (erreurs plutôt dûes au script et donc aux fichiers d'entrée/sortie notamment).

<h2>Correction des manifests produits par preziiif</h2>

Le script **Jupyter Notebook**  `correction_manifest_image_service.ypnb` permet de corriger l'ensemble des manifests produits par preziiif :
1. Il ré-harmonise les id des canvas (`"https://fontegaia.huma-num.fr/iiif/" + <id_document> + "/canvas/p" + <num_de_page>`) (important pour la génération des sommaires).
2. Il assure la compatibilité de l'API Image 3 utilisée par Nakala avec l'API presentation 2.0 utilisée par Universal Viewer, en modifiant les propriétés du service des canvas.

Le script traite l'ensemble des fichiers `manifest.json` du dossier `/output` :il n'y a ensuite plus qu'à les déposer sur le serveur.



<details> <summary markdown="span"><b>Détail compatibilité APIs IIIF</b></summary>
The requirements of this backward compatibility are documented in the [#Service section of the 3.0 Presentation API](https://iiif.io/api/presentation/3.0/#service) and the [#Linking Properties section of the 3.0 Image API](https://iiif.io/api/image/3.0/#58-linking-properties).
To sum it up, the only requirements to make the Presentation API 2.1 include resources provided through the 3.0 Image API is to modify the properties and values of the service object which every resource must have, so that
* "@id" is replaced by the property "id"
* The property "@context" : and its value "http://iiif.io/api/image/2/context.json" are replaced by a property "type" : with  "ImageService3" as its value.
</details>

:warning: **Non, en fait alles gut**

* 384212101_122319_1911_181 = Le martyre de Saint Sebastien par Gabriele ... => **Autre édition = 384212101_32153**
* 384212101_122319_1927_196 = la torche sous le boisseau traduit par André Doderet (1928 pour cette édition) = 384212101_XF4070 **pas un doublon mais bien une autre édition**
* 384212101_XE354950 = Laudi del cielo ecc.
* 384212101_XF350898 = les victoires mutilées (tj Gabriele)




<h2>Génération des tables des matières</h2>


Dans `/toc` **Compléter plus tard**
1. `toc_pdftk.php` : fichier php extrait du plugin https://github.com/JBPressac/Plugin-PdfToc (modifié pour une génération plus détaillée)
2. à partir du fichier .csv produit, lancer `toc_iiif.ypnb` : produit une propriété JSON `"structures" : []` (la sortir de l'objet vide `{ structures : []}` dans laquelle elle est produite).
3. Réintroduire la TOC dans le Manifest (au même niveau que séquence, manuellement, ou encore un script en python, avant de l'intégrer à l'outil de génération des manifests ?).


- Structure de la TOC si convertie en char. pour édition manuelle dans Omeka (?)

```
<hiérarchie div> <titre de la div>       <page début> <page fin> <liste des div enfants (sont num de 0 à X, 0 est générée auto et représente la table des matières)>
2         |     I. SERIE DELLE EDIZIONI...  | 28       |215    |     8,9,10,11,12
3         |     Edizioni del XV. secolo     | 28       |74     |     
3         |     Edizioni del secolo XVI     | 75       |116    |     
```

* plus tard : forker complètement Plugin PDF TOC pour permettre une saisie manuelle / une analyse du PDF ?


<h2> Exemples de commandes pour dépôt sur le serveur</h2>

- En local (en supposant que les manifestes ont été créé dans `manifests_json/` :
```
rm manifests_json.zip #optionnel
zip -r manifests_json.zip
manifests_json
scp -r manifests_json.zip  fontegaia.huma-num.fr:~
```
- Se connecter au serveur :
```
rm -fr manifests_json #optionnel
unzip manifests_json.zip
rm -fr /data/fonte-gaia-bib-c/files/manifests_json/
cp -r manifests_json /data/fonte-gaia-bib-c/files/
```
