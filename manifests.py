from iiif_prezi.factory import ManifestFactory, ConfigurationError, Image, RequirementError, ImageService, Service, \
    BaseMetadataObject
from iiif_prezi.util import is_http_uri
import os
import json
import csv
from typing import Any
from urllib.request import Request
from urllib.request import urlopen
from urllib.error import HTTPError

'''
# UNCOMMENT IF USING CANTALOUPE
class MyManifestFactory(ManifestFactory):
    """Patched for: https://github.com/medusa-project/cantaloupe/issues/48#issuecomment-252688164
    """

    def __init__(self, *args, slash_substitute="/", **kwargs):
        super().__init__(*args, **kwargs)
        self._slash_substitute = slash_substitute

    def image(self, ident, label="", iiif=False, region='full', size='full'):
        """Create an Image."""
        if not ident:
            raise RequirementError(
                "Images must have a real identity (Image['@id'] cannot be empty)")
        return MyImage(self, ident, label, slash_substitute=self._slash_substitute, iiif=iiif, region=region,
                       size=size)  # Only differing line: we use MyImage and pass the slash


class MyImage(Image):
    """Patched for: https://github.com/medusa-project/cantaloupe/issues/48#issuecomment-252688164
    """

    def __init__(self, factory, ident, label, slash_substitute="/", iiif=False, region='full', size='full'):
        """Initialize Image resource."""
        self._factory = factory
        self.type = self.__class__._type
        self.label = ""
        self.format = ""
        self.height = 0
        self.width = 0
        self._identifier = ""
        self._slash_substitute = slash_substitute
        if label:
            self.set_label(label)

        if iiif:
            # add IIIF service -- iiif is version or bool
            # ident is identifier
            self.service = MyImageService(factory, ident, slash_substitute=slash_substitute)

            if factory.default_image_api_version[0] == '1':
                self.id = factory.default_base_image_uri + slash_substitute + \
                          ident + '/%s/%s/0/native.jpg' % (region, size)
            else:
                self.id = factory.default_base_image_uri + slash_substitute + \
                          ident + '/%s/%s/0/default.jpg' % (region, size)
            self._identifier = ident
            self.format = "image/jpeg"
        else:
            # Static image
            # ident is either full URL or filename
            if is_http_uri(ident):
                self.id = ident
            else:
                factory.assert_base_image_uri()
                self.id = factory.default_base_image_uri + ident

    def set_hw(self, h, w):
        """Set height and width to specified values."""
        self.height = h
        self.width = w

    def set_hw_from_iiif(self):
        """Set height and width from IIIF Image Information.
        """
        if not self._identifier:
            raise ConfigurationError(
                "Image is not configured with IIIF support")
        requrl = self._factory.default_base_image_uri + \
                 self._slash_substitute + self._identifier + '/info.json'
        try:
            if self._factory.image_auth_token:
                print(books)
                req = Request(requrl, headers={
                    'Authorization': self._factory.image_auth_token})
            else:
                req = Request(requrl)
            fh = urlopen(req)
            data = fh.read().decode('utf-8')
            fh.close()
        # Ligne ajoutee pour gerer les fichiers corrompus
        except HTTPError:
            return False
        except:
            raise ConfigurationError(
                "Could not get IIIF Info from %s" % requrl)

        try:
            js = json.loads(data)
            self.height = int(js['height'])
            self.width = int(js['width'])
        except:
            print(data)
            raise ConfigurationError(
                "Response from IIIF server did not have mandatory height/width")


class MyImageService(ImageService):
    """Patched for: https://github.com/medusa-project/cantaloupe/issues/48#issuecomment-252688164
    Image Service specialization of Service object in Presentation API.
    """

    def __init__(self, factory, ident, label="", context="", profile="", slash_substitute="/"):
        """Initialize Image Service."""
        self._factory = factory
        self._slash_substitute = slash_substitute
        if not is_http_uri(ident):
            # prepend factory.base before passing up
            ident = factory.default_base_image_uri + self._slash_substitute + ident

        BaseMetadataObject.__init__(self, factory, ident, label)

        if not context:
            self.context = factory.default_image_api_context
        else:
            self.context = context
        if not profile and factory.default_image_api_level != -1:
            self.profile = factory.default_image_api_profile
        elif profile:
            self.profile = profile
'''

def get_metadata(csv_file):
    books = dict()
    with open(csv_file, "r", encoding="utf8") as csv_metadata:
        metadata_books = csv.reader(csv_metadata, delimiter=",", quotechar='"')
        title_row = next(metadata_books)
        for element in title_row:
            # recuperer l'id de l'identifier
            if element == "dcterms:identifier":
                id_index = title_row.index(element)
        for book in metadata_books:
            books[book[id_index]] = {}
            for i in range(len(title_row)):
                books[book[id_index]][title_row[i]] = book[i]
    return books

def filename_to_handle(csv_file):
    with open(csv_file, "r", encoding="utf8") as csv_file:
        data = csv.reader(csv_file, delimiter=",", quotechar='"')
        filename2handle = dict()
        for row in data:
            filename2handle[row[1]] = row[0]
    return filename2handle

def write_log(error_folder, error_file):
    """
    :param error_folder:
    :param error_file:
    :return:
    """
    log = "DOSSIERS MAL ORGANISES \n"
    for error in error_folder:
        if error is not "":
            log += error + "\n"
    log += "FICHIERS CORROMPUS \n"
    for errors in error_file:
        for error in errors:
            log += str(error) + "\n"
    with open("log.txt", "w") as log_file:
        log_file.write(log)


def createManifest(base_uri, book, loc_images, stock_manifest, metadata):
    print('\n' + book, 'Output: ' + stock_manifest, sep='\n  ')
    """Creates manifest file of a book manifest.json
    """
    filename2handle = filename_to_handle("input/filename2handle.csv")
    errors_jpeg = []
    errors_folder = ""
    images = [img for img in os.listdir(loc_images + book.strip() + "/JPEG") if ".jpg" in img and img != "Titre.jpg"]
    if images == []:
        print('  ATTENTION: Pas d\'images dans le dossier : ' + loc_images + book.strip() + "/JPEG");
        errors_folder = book
    else:
        print('  IMG trouvées : ' + str(len(images)) + " files");
        # fac = ManifestFactory(slash_substitute="%2F")
        fac = ManifestFactory()
        
        # Adresse du serveur 
        fac.set_base_prezi_uri(base_uri)
        
        # Adresse du dossier avec les images
        # fac.set_base_image_uri(base_uri + book + "%2FJPEG/")
        fac.set_base_image_uri(base_uri)
        fac.set_iiif_image_info(2.0, 2)  # Version, ComplianceLevel
        fac.set_debug("warn")
        # Creation manifest
        manifest = fac.manifest(label=metadata["dcterms:title"]);
        manifest.set_metadata(metadata);
        manifest.description = metadata["dcterms:title"]
        manifest.viewingDirection = "left-to-right";
        seq = manifest.sequence();
        images.sort()
        for filename in images:
            if filename in filename2handle:
                # Create a canvas
                cvs = seq.canvas(ident=str(filename), label=str(filename))
                # Create an annotation on the Canvas
                # anno = cvs.annotation(base_uri + book + "%2FJPEG%2F" + filename)
                anno = cvs.annotation(base_uri + filename2handle[filename])
                # img = anno.image(str(filename), iiif=True)
                img = anno.image(filename2handle[filename], iiif=True)
                #cvs.annotationList("http://sid-omeka.u-ga.fr/omeka-s/files/annotations/" + filename.replace(".jpg", ".json"))
                try :
                    img.set_hw_from_iiif()
                    cvs.height = img.height
                    cvs.width = img.width
                except ConfigurationError:
                    cvs.height = 1
                    cvs.width = 1
                    errors_jpeg.append(filename)
            else:
                print("  ATTENTION: Pas de handle disponible pour l'image " + filename)
                return "  PASSAGE AU SUIVANT\n"
        data = manifest.toString(compact=False)
        if not os.path.exists(stock_manifest):
            os.makedirs(stock_manifest)
        with open(stock_manifest + "/manifest.json", "w") as file_manifest:
            file_manifest.write(data)
        return errors_folder, errors_jpeg
                

# PARTIE A MODIFIER 
# adresse du dossier avec tous les dossiers ouvrages
#loc_images = 'Z:/Diffusion/'
#loc_images = '/home/annegf/Bureau/partageUGA/diffusion/'
loc_images = '/media/annegf/VERBATIM HD/Images/FonteGaia/diffusion/'
# base_uri = "http://sid-sidoc.u-ga.fr:8080/iiif/2/"
base_uri = "https://www.nakala.fr/iiif/"
metadata="input/metadonnees.csv"
errors = []
books = get_metadata(metadata)
#books = get_metadata("metadonnees_l53_fin.csv")
for book, metadata in books.items():
    # Adresse de la ou l'on veut ranger le manifest (a mettre directement sur Summer a terme)
    #stock_manifest = 'H:/Fonte Gaia Dev/IIIF/manifests_test2/' + book.strip()
    stock_manifest = '/home/annegf/Data/owncloud/Projets/FonteGaia/from_Samuel_docs_FG_Omeka/IIIF/output/manifests_json/' + book.strip()

    # uri du serveur image
    errors.append(createManifest(base_uri=base_uri, book=book, loc_images=loc_images,
                                 stock_manifest=stock_manifest, metadata=metadata))
write_log([error[0] for error in errors], [error[1] for error in errors])

# Si pas de dossier equivalent dans le fichier metadonnees il faut preciser l'erreur !
