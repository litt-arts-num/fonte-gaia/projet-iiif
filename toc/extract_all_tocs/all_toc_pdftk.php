<?php


$handle = fopen("ids_fg.csv", "r");
 while (($data = fgetcsv($handle, 10000, ",")) !== false) {
     $documentList[] = $data[0];
 }
     fclose($handle);

//var_dump($documentList);

// foreach ($csv as $key => $value) {
//     $itemList[]["folder"] = "/media/theo/VERBATIM HD/Images/FonteGaia/diffusion/" . $value;
//     $itemList[$key]["pdf"] = $value . ".pdf";
// }
//
// var_dump($itemList);

foreach ($documentList as $key => $document) {
    $disk = "'VERBATIM HD'";
    $path = "/media/theo/$disk/Images/FonteGaia/diffusion/$document/$document.pdf";

    $dump_data =  shell_exec("pdftk $path dump_data_utf8");

    //var_dump($dump_data);

    if (is_string($dump_data)) {
        //prepare Toc. Tested with PDFtk 2.02.

        $pagesearch = preg_match("/NumberOfPages:\s(.*)/", $dump_data, $match);
        $pageNb = intval($match[1]);
        //var_dump($pageNb);

        //  $pagenb = preg_match("/^NumberOfPages:\s.+$/", $dump_data, $match);

        //$nbloc = strpos($dump_data, "NumberOfPages")+15;
        // $nb = substr($dump_data, $nbloc, ,);


        //var_dump($nb);

        //$dump_data = preg_replace("/^.*(NumberOfPages.*)$/isU", "$1", $dump_data);
        $dump_data = preg_replace("/^.*(Bookmark.*)$/isU", "$1", $dump_data);
        $dump_data = preg_replace("/BookmarkBegin\n/isU", '', $dump_data);
        $dump_data = preg_replace("/(^.*)PageMediaBegin.*$/isU", "$1", $dump_data);
        $dump_data_array = preg_split("/\n/", $dump_data);

        //array_push($dump_data_array, "");
        // $dump_data_array = array_splice($dump_data_array, 0,   -2);
        //var_dump(sizeof($dump_data_array));
        //var_dump($dump_data_array);




        $toc = "";
        for ($i = 0; $i <= sizeof($dump_data_array)-3; $i+=3) {
            // Bookmarks created with Acrobat Pro may contain carriage returns
            // i.e. \r (displayed &#13; when using PDFtk dump_data instead of PDFtk dump_data_utf8).
            $bm_title = str_replace("BookmarkTitle: ", "", preg_replace("/\r/", "", $dump_data_array[$i]));
            // Deletion of spare spaces before and after the titles (e.g. " Le Horla ").
            $bm_title = preg_replace("/ *$/", "", $bm_title);
            $bm_title = preg_replace("/^ */", "", $bm_title);
            $bm_level = str_replace("BookmarkLevel: ", "", $dump_data_array[$i+1]);
            $bm_page = str_replace("BookmarkPageNumber: ", "", $dump_data_array[$i+2]);
            if ($toc != "") {
                $toc .= "\n";
            }
            if (($bm_level != "") and ($bm_title != "") and ($bm_page != "")) {
                $toc .= $bm_level."|".$bm_title."|".$bm_page;
            }
        }
        //var_dump($toc);

        $first = true;
        $tableToc = [];

        ## Init our tableToc with the Table of content div, encompassing all others
        ## will be the parent div of all div of level 1
        $tableToc[0] = [
        "level" =>  "0",
        "title" => "Table of contents",
        "page" => 1,
        "extent" => $pageNb,
        "children" => [],
      ];



        $tablen = explode("\n", $toc);
        foreach ($tablen as $line) {
            $cells = explode("|", $line);

            $tableToc[] = [
            "level" =>  $cells[0],
            "title" => $cells[1],
            "page" => $cells[2],
            "extent" => 0,
            "children" => [],
          ];


            $precedingLevel = $cells[0];

            $first=false;
        }

        //var_dump($tableToc);

        // preliminariesTableToc is used to refined the ranges hierarchy in the future manifest
        // When an encapsulating div has pages unique to it, they are added as an [intro] subdiv
        $preliminariesTableToc = [];
        $y = 0;
        for ($i = 0; $i <=sizeof($tableToc)-1; $i+=1) {
            $preliminariesTableToc[$y] = $tableToc[$i];
            if ($i != 0 and $i < sizeof($tableToc)-1) { // prevent array offset
                // also ignores Table of contents div which is selected when $i == 0
                if ($tableToc[$i+1]["level"] > $tableToc[$i]["level"] and $tableToc[$i+1]["page"] != $tableToc[$i]["page"]) {
                    // if the following div is a lower div AND if the following div does not start on the same page (meaning that the encapsulating div has no peroper pages)
                    $introDiv = [
                      "level" => strval(intval($tableToc[$i]["level"]) + 1),
                      "title" => "[intro] " . $tableToc[$i]["title"],
                      "page" => strval($tableToc[$i]["page"]) ,
                      "extent" => 0,
                      "children" => [],
                    ];
                    // insert new introDiv into preliminariesTableToc
                    $preliminariesTableToc[$y+1] = $introDiv;
                    $y +=1;
                }
            }
            $y +=1;
        }


        //var_dump($preliminariesTableToc);

        $tableToc = $preliminariesTableToc;
        //var_dump($tableToc);


        //    RE-TRANSFORMATION EN TEXTE
        // $linetext = [];
        // foreach ($tableToc as $line) {
        //     $line["children"] = implode($line["children"], ",");
        //     $linetext[] = implode($line, "|");
        // }
        // var_dump(implode($linetext, "\n"));


        for ($i = 1; $i <= sizeof($tableToc)-1; $i+=1) {
            // start at 1 to avoid TOC which is tableToc[0]

            //var_dump($i);
            // if (!$tableToc[$i]["level"]) {
            //     var_dump("ERROR");      // should not keep this like that
            //     var_dump($tableToc[$i]);
            //     break;
            // }

            $level = intval($tableToc[$i]["level"]);

            # appends level 1 div keys to TOC (level 0 )
            if ($level == 1) {
                array_push($tableToc[0]["children"], $i);
            }

            $counter = $i+1;
            // echo $i;
            // echo $counter;
            while ($counter <= sizeof($tableToc)-1) {
                if ($level < intval($tableToc[$counter]["level"])) {
                    array_push($tableToc[$i]["children"], $counter);
                    $counter+=1 ;
                //echo($counter);
                } else {
                    //var_dump($tableToc[$i]);
                    break;
                }
            }
            if (empty($tableToc[$i]["children"])) {
                if ($i == sizeof($tableToc)-1) {
                    $tableToc[$i]["extent"] = $pageNb - intval($tableToc[$i]["page"]);
                } else {
                    $tableToc[$i]["extent"] = $tableToc[$i+1]["page"] -1;
                }
                if ($i == sizeof($tableToc)-1) {
                    $tableToc[$i]["extent"] = $pageNb;
                }
            } else { // if tableToc has children, we take the div after the last to know its full extent
                $pointerTarget = end($tableToc[$i]["children"]) ;
                if ($pointerTarget < sizeof($tableToc)-1) {
                    $tableToc[$i]["extent"] = $tableToc[$pointerTarget+1]["page"] -1;
                } else { // except if this div wraps everything until the end : then it laster page is the last of the document
                    $tableToc[$i]["extent"] = $pageNb;
                }
            }
        }

        // date_default_timezone_set('Europe/Paris');
        // $date = date("Y-m-d_H:i");
        // $filename =  "export_" . str_replace(":", "h", $date) . ".csv";





        var_dump($document); /// to debug


        $handle = fopen("/home/theo/Documents/FonteGaia/IIIF/projet-iiif/toc/extract_all_tocs/output/$document.csv", 'w');

        $columns = [
        "id",
        "level",
        "title",
        "page" ,
        "extent" ,
        "children"
      ];

        fputcsv($handle, $columns);


        foreach ($tableToc as $id => $div) {
            $data = [
            $id,
            $div["level"],
            $div["title"],
            $div["page"],
            $div["extent"],
            implode($div["children"], ",")
        ];
            fputcsv($handle, $data);
        }

        rewind($handle);
        fclose($handle);
    }
}
